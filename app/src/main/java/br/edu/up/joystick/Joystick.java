package br.edu.up.joystick;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class Joystick extends View {

	private double x, y;
	private Listener listener;
	private Paint pincelDaArea;
	private Paint pincelDoControle;
	private int raioDoControle;
	private int limiteDoControle;
	private int margemInterna = 1;
	private int sensibilidade = 1;

	public interface Listener {
		void aoMover(int pan, int tilt);
		void aoSoltar();
	}

	public Joystick(Context context) {
		super(context);
		iniciarControle();
	}

	public Joystick(Context context, AttributeSet attrs) {
		super(context, attrs);
		iniciarControle();
	}

	private void iniciarControle() {
		setFocusable(true);

		pincelDaArea = new Paint(Paint.ANTI_ALIAS_FLAG);
		pincelDaArea.setStyle(Paint.Style.FILL_AND_STROKE);
		pincelDaArea.setColor(Color.WHITE);
		pincelDaArea.setAlpha(7);

		pincelDoControle = new Paint(Paint.ANTI_ALIAS_FLAG);
		pincelDoControle.setStyle(Paint.Style.FILL_AND_STROKE);
	}

	public void setListener(Listener listener) {
		this.listener = listener;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int largura = medir(widthMeasureSpec);
		int altura = medir(heightMeasureSpec);
		int d = Math.min(largura, altura);

		raioDoControle = (int)(d * 0.25);
		limiteDoControle = raioDoControle;
		
		setMeasuredDimension(d, d);
	}

	private int medir(int medida) {
		int valor = 0;
		int modo = MeasureSpec.getMode(medida);
		int tamanho = MeasureSpec.getSize(medida);
		if (modo == MeasureSpec.UNSPECIFIED) {
			valor = 200;
		} else {
			valor = tamanho;
		}
		return valor;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		int px = getMeasuredWidth() / 2;
		int py = getMeasuredHeight() / 2;
		int raio = Math.min(px, py);

		canvas.drawCircle(px, py, raio - margemInterna, pincelDaArea);

		int cx = (int) x + px;
		int cy = (int) y + py;
		RadialGradient gradiente = new RadialGradient(cx, cy, raioDoControle, Color.RED,
				Color.BLACK, android.graphics.Shader.TileMode.CLAMP);
		pincelDoControle.setShader(gradiente);
		canvas.drawCircle(cx, cy, raioDoControle, pincelDoControle);
		canvas.save();
	}

	@Override
	public boolean onTouchEvent(MotionEvent evento) {

		int acao = evento.getAction();

		if (acao == MotionEvent.ACTION_MOVE) {
			int px = getMeasuredWidth() / 2;
			int py = getMeasuredHeight() / 2;
			int raio = Math.min(px, py) - limiteDoControle;

			x = (evento.getX() - px);
			x = Math.max(Math.min(x, raio), -raio);

			y = (evento.getY() - py);
			y = Math.max(Math.min(y, raio), -raio);

			if (listener != null) {
				listener.aoMover((int) (x / raio * sensibilidade), (int) (y / raio * sensibilidade));
			}

			invalidate();

		} else if (acao == MotionEvent.ACTION_UP) {

			voltarAoCentro();
		}
		return true;
	}

	private void voltarAoCentro() {

		Handler handler = new Handler();
		int numeroDeQuadros = 5;
		final double intervaloX = (0 - x) / numeroDeQuadros;
		final double intervaloY = (0 - y) / numeroDeQuadros;

		for (int i = 0; i < numeroDeQuadros; i++) {
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					x += intervaloX;
					y += intervaloY;
					invalidate();
				}
			}, i * 40);
		}

		if (listener != null) {
			listener.aoSoltar();
		}
	}
}