package br.edu.up.joystick;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

public class Labirinto extends View {

  private Bitmap fundo;
  private Bitmap pacman;
  private Paint pincelFundo;
  private Paint pincelPacman;

  int pacx = 150;
  int pacy = 150;
  int incrementoLateral = 10;
  int incrementoVertical = 0;
  int multiplicador = 10;

  public Labirinto(Context context) {
    super(context);
    iniciarLabirinto();
  }

  public Labirinto(Context context, AttributeSet attrs) {
    super(context, attrs);
    iniciarLabirinto();
  }

  private void iniciarLabirinto() {
    fundo = BitmapFactory.decodeResource(getResources(), R.drawable.pacman350);
    pacman = BitmapFactory.decodeResource(getResources(), R.drawable.pac);
    pincelFundo = new Paint();

    final Handler handler = new Handler();
    Runnable runnable = new Runnable() {
      public void run() {
        while (true) {
          try {
            Thread.sleep(100);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          handler.post(new Runnable() {
            public void run() {
              pacx += incrementoLateral;
              pacy += incrementoVertical;
              invalidate();
            }
          });
        }
      }
    };
    new Thread(runnable).start();
  }

  public void mudarDirecao(int x, int y) {
    incrementoLateral = x * multiplicador;
    incrementoVertical = y * multiplicador;
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    canvas.drawBitmap(fundo, 0, 0, pincelFundo);
    canvas.drawBitmap(pacman, pacx, pacy, pincelPacman);
  }
}