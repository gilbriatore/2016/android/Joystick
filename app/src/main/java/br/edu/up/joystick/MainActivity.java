package br.edu.up.joystick;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

  Labirinto labirinto;
  Joystick joystick;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    labirinto = (Labirinto) findViewById(R.id.labirinto);
    joystick = (Joystick) findViewById(R.id.joystick);
    joystick.setListener(_listener);
  }

  private Joystick.Listener _listener = new Joystick.Listener() {

    @Override
    public void aoMover(int x, int y) {
      labirinto.mudarDirecao(x, y);
    }

    @Override
    public void aoSoltar() {
    }
  };

}
